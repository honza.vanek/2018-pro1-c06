package students;

public class Student {
    private final String firstName, lastName;
    private final Time time;

    public Student(String firstName, String lastName, Time time) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.time = time;
    }

    String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Time getTime() {
        return this.time;
    }

 }
