package students;

import exceptions.InvalidIndexException;

public interface Classroom {
    Classroom add(Student student);
    Student get(int i) throws InvalidIndexException;
    Student fastestStudent();
}
