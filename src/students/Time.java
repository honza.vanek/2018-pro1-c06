package students;

public class Time {
    private final int hour, minute;

    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    int timeInMinutes() {
        return this.hour * 60 + this.minute;
    }

}
