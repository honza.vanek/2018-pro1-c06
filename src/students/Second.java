package students;

import collection.List;
import collection.ListArray;
import collection.ListLinked;
import exceptions.InvalidIndexException;

import java.util.Random;
import java.util.Scanner;

class Second {
	private static Random random = new Random();
	private static Scanner input = new Scanner(System.in);


	//A: Vypis studenta
	//I: Promenne s udaji o studentovi
	//O: Vypis do konzole
		   //O:            //A:     //I:
	static void/*FUJ*/ printStudent(
			final Student student) {
		final Time time = student.getTime();
		System.out.println("Jmeno: " + student.getFirstName() +
				"\nPrijmeni: " + student.getLastName() +
				"\nCas: " + time.getHour() + ":" + time.getMinute()
		);
	}

	static String inputNextWithPrompt(final String text) {
		System.out.println(text);
		return input.next();
	}

	static int inputNextIntWithPrompt(final String text) {
		System.out.println(text);
		return input.nextInt();
	}

	static Student inputStudent(final String whichStudent) {
		System.out.println("=== Zadej udaje " + whichStudent + " studenta:");
		return new Student(
				inputNextWithPrompt("Zadej krestni jmeno:"),
				inputNextWithPrompt("Zadej prijmeni:"),
				new Time(
					inputNextIntWithPrompt("Zadej hodinu:"),
					inputNextIntWithPrompt("Zadej minutu:")
				)
		);
	}

	public static void main(String[] args) {
		//A: Nacist krestni jmeno, prijmeni, hodinu, minutu
		//   dvou studentu
		//I: Udaje zadane uzivatelem
		//O: Promenne s udaji studentu

		final List classroomEmpty;
		if (random.nextInt(10) >= 5)
			classroomEmpty = new ListArray();
		else
			classroomEmpty = new ListLinked();

		final List classroom = classroomEmpty
					.add(inputStudent("prvniho"))
					.add(inputStudent("druheho"))
					.add("tretiho"/*inputStudent("tretiho")*/);
		System.out.println("======== Kontrolni vypis ========");
		try {
			printStudent((Student)classroom.get(0));
			printStudent((Student)classroom.get(1));
			printStudent((Student)classroom.get(2));
		} catch (InvalidIndexException e) {
			System.out.println("neplatny index");
			e.printStackTrace();
		}


		//A: Zjistit udaje studenta, ktery odevzdal prvni
		//I: Promenne s udaji o studentech (vystup z predch.)
		//O: Promenne s udaji o studentovi
//		final Student student = classroom.fastestStudent();
//
//
		System.out.println("======== Prvni odevzdal ========");
//		//A: Vypis studenta
//		//I: Promenne s udaji o studentovi
//		//O: Vypis do konzole
//		printStudent(student);

	}
}
