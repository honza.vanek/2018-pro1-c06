package students;

import exceptions.InvalidIndexException;

import java.util.*;

class First {
	private static Random random = new Random();
	private static Scanner input = new Scanner(System.in);


	//A: Vypis studenta
	//I: Promenne s udaji o studentovi
	//O: Vypis do konzole
		   //O:            //A:     //I:
	static void/*FUJ*/ printStudent(
			final Student student) {
		final Time time = student.getTime();
		System.out.println("Jmeno: " + student.getFirstName() +
				"\nPrijmeni: " + student.getLastName() +
				"\nCas: " + time.getHour() + ":" + time.getMinute()
		);
	}

	static String inputNextWithPrompt(final String text) {
		System.out.println(text);
		return input.next();
	}

	static int inputNextIntWithPrompt(final String text) {
		System.out.println(text);
		return input.nextInt();
	}

	static Student inputStudent(final String whichStudent) {
		System.out.println("=== Zadej udaje " + whichStudent + " studenta:");
		return new Student(
				inputNextWithPrompt("Zadej krestni jmeno:"),
				inputNextWithPrompt("Zadej prijmeni:"),
				new Time(
					inputNextIntWithPrompt("Zadej hodinu:"),
					inputNextIntWithPrompt("Zadej minutu:")
				)
		);
	}

	public static void main(String[] args) {
		//A: Nacist krestni jmeno, prijmeni, hodinu, minutu
		//   dvou studentu
		//I: Udaje zadane uzivatelem
		//O: Promenne s udaji studentu

		final Classroom classroomEmpty;
		if (random.nextInt(10) >= 5)
			classroomEmpty = new ClassroomArray();
		else
			classroomEmpty = new ClassroomLinked();

		final Classroom classroom = classroomEmpty
					.add(inputStudent("prvniho"))
					.add(inputStudent("druheho"))
					.add(inputStudent("tretiho"));
		System.out.println("======== Kontrolni vypis ========");
		try {
			printStudent(classroom.get(0));
			printStudent(classroom.get(1));
			printStudent(classroom.get(2));
		} catch (InvalidIndexException e) {
			System.out.println("neplatny index");
			e.printStackTrace();
		}


		//A: Zjistit udaje studenta, ktery odevzdal prvni
		//I: Promenne s udaji o studentech (vystup z predch.)
		//O: Promenne s udaji o studentovi
		final Student student = classroom.fastestStudent();
//
//
		System.out.println("======== Prvni odevzdal ========");
//		//A: Vypis studenta
//		//I: Promenne s udaji o studentovi
//		//O: Vypis do konzole
		printStudent(student);

	}
}
