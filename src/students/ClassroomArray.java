package students;

import exceptions.InvalidIndexException;

public class ClassroomArray implements Classroom {
    private final Student[] students;

    public ClassroomArray() {
        students = new Student[0];
    }

    private ClassroomArray(final Student[] students) {
        this.students = students;
    }

    public ClassroomArray add(final Student student) {
        final Student[] newStudents = new Student[students.length + 1];
        for (int i = 0; i < students.length; i++)
            newStudents[i] = students[i];
        newStudents[newStudents.length - 1] = student;
        return new ClassroomArray(newStudents);
    }

    public Student get(final int i) throws InvalidIndexException {
        if (i >= 0 && i < students.length)
            return students[i];
        throw new InvalidIndexException();
    }


    private Student fastestStudent(final Student currentFastest, final int i) {
        if (i == students.length)
            return currentFastest;
        return fastestStudent(
                currentFastest.getTime().timeInMinutes()
                    < students[i].getTime().timeInMinutes() ?
                 currentFastest : students[i],
                i + 1);
    }

    public Student fastestStudent() {
        return fastestStudent(students[0], 1);
    }

//    public Student fastestStudent() {
//        Student fastest = students[0];
//        for (int i = 1; i < students.length; i++)
//            if (fastest.getTime().timeInMinutes()
//                    > students[i].getTime().timeInMinutes())
//                fastest = students[i];
//        return fastest;
//    }
}
