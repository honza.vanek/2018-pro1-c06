package students;

import exceptions.InvalidIndexException;

public class ClassroomLinked implements Classroom {

    private class Node {
        private final Student student;
        private final Node next;

        public Node(Student student, Node next) {
            this.student = student;
            this.next = next;
        }
    }

    private final Node head;
    private final int size;

    public ClassroomLinked() {
        head = null;
        size = 0;
    }

    private ClassroomLinked(final Node head, final int size) {
        this.head = head;
        this.size = size;
    }

    public Classroom add(final Student student) {
        return new ClassroomLinked(new Node(student, head), size + 1);
    }

    private Student get(final Node currentNode, final int j) {
        if (j == 0)
            return currentNode.student;
        return get(currentNode.next, j - 1);
    }


    public Student get(final int i) throws InvalidIndexException {
        if (i >= 0 && i < size)
            return get(head, size - 1 - i);
        throw new InvalidIndexException();
    }

    private Student fastestStudent(
            final Student currentFastest, final Node currentNode) {
        if (currentNode == null)
            return currentFastest;
        return fastestStudent(
                currentFastest.getTime().timeInMinutes()
                        < currentNode.student.getTime().timeInMinutes() ?
                        currentFastest : currentNode.student,
                currentNode.next);
    }

    public Student fastestStudent() {
        return fastestStudent(head.student, head.next);
    }

}
