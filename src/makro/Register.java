package makro;

import exceptions.InvalidIndexException;

public interface Register {
    Register add(Item item);
    Item get(int i) throws InvalidIndexException;
    double totalPrice();
}
