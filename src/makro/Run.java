package makro;

import java.util.Random;
import java.util.Scanner;

class Run {
	private static Random random = new Random();
	private static Scanner input = new Scanner(System.in);


	public static void main(String[] args) {
		final VAT standardRate = new VAT(0.21),
					reducedRate = new VAT(0.15);
		final Dept
				groceries = new Dept("groceries"),
				toiletries = new Dept("toiletries");
		final Discount
				noDiscount = new Discount("none", 0),
				summerSale = new Discount("summer sale", 0.1);
		final Product
				bun = new Product(
					"bun01","bun roll", 1.9,
					groceries, reducedRate),
				soap = new Product(
						"soap01","liquid soap", 25.9,
						toiletries, standardRate);

		final Register registerEmpty = new RegisterLinked();
		final Register register = registerEmpty
				.add(new Item(5, noDiscount, bun))
				.add(new Item(1.5, summerSale, soap));

		System.out.println(register.totalPrice());
	}
}
