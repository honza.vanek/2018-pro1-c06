package makro;

import exceptions.InvalidIndexException;

public class RegisterArray implements Register {
    private final Item[] items;

    public RegisterArray() {
        items = new Item[0];
    }

    private RegisterArray(final Item[] items) {
        this.items = items;
    }

    public RegisterArray add(final Item item) {
        final Item[] newItems = new Item[items.length + 1];
        for (int i = 0; i < items.length; i++)
            newItems[i] = items[i];
        newItems[newItems.length - 1] = item;
        return new RegisterArray(newItems);
    }

    public Item get(final int i) throws InvalidIndexException {
        if (i >= 0 && i < items.length)
            return items[i];
        throw new InvalidIndexException();
    }


    private double totalPrice(final double currentTotal, final int i) {
        if (i == items.length)
            return currentTotal;
        return totalPrice(
                currentTotal + items[i].finalPrice(),
                i + 1);
    }

    @Override
    public double totalPrice() {
        return totalPrice(items[0].finalPrice(), 1);
    }

}
