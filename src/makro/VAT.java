package makro;

public class VAT {
    private final double value;

    public VAT(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
