package makro;

public class Item {
    private final double quantity;
    private final Discount discount;
    private final Product product;

    public Item(double quantity, Discount discount, Product product) {
        this.quantity = quantity;
        this.discount = discount;
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public Discount getDiscount() {
        return discount;
    }

    public Product getProduct() {
        return product;
    }

    public double finalPrice() {
        return getQuantity() * getProduct().priceInclVAT()
                * (1 - getDiscount().getValue());
    }
}
