package makro;

public class Product {
    private final String id, label;
    private final double unitPrice;
    private final Dept dept;
    private final VAT vat;

    public Product(String id, String label, double unitPrice, Dept dept, VAT vat) {
        this.id = id;
        this.label = label;
        this.unitPrice = unitPrice;
        this.dept = dept;
        this.vat = vat;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public Dept getDept() {
        return dept;
    }

    public VAT getVat() {
        return vat;
    }

    public double priceInclVAT() {
        return getUnitPrice() * (1 + getVat().getValue());
    }
}
