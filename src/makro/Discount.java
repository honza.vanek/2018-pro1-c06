package makro;

public class Discount {
    private final String description;
    private final double value;

    public Discount(String description, double value) {
        this.description = description;
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public double getValue() {
        return value;
    }
}
