package makro;

public class Dept {
    private final String name;

    public Dept(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
