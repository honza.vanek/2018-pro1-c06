package collection;

import exceptions.InvalidIndexException;

public class ListLinked implements List {

    private class Node {
        private final Object item;
        private final Node next;

        public Node(Object item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

    private final Node head;
    private final int size;

    public ListLinked() {
        head = null;
        size = 0;
    }

    private ListLinked(final Node head, final int size) {
        this.head = head;
        this.size = size;
    }

    public ListLinked add(final Object item) {
        return new ListLinked(new Node(item, head), size + 1);
    }

    private Object get(final Node currentNode, final int j) {
        if (j == 0)
            return currentNode.item;
        return get(currentNode.next, j - 1);
    }


    public Object get(final int i) throws InvalidIndexException {
        if (i >= 0 && i < size)
            return get(head, size - 1 - i);
        throw new InvalidIndexException();
    }

//    private double totalPrice(
//            final double currentTotal, final Node currentNode) {
//        if (currentNode == null)
//            return currentTotal;
//        return totalPrice(
//                currentTotal + currentNode.item.finalPrice(),
//                currentNode.next);
//    }
//
//    @Override
//    public double totalPrice() {
//        return totalPrice(head.item.finalPrice(), head.next);
//    }

}
