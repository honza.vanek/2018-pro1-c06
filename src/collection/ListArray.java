package collection;

import exceptions.InvalidIndexException;

public class ListArray implements List {
    private final Object[] items;

    public ListArray() {
        items = new Object[0];
    }

    private ListArray(final Object[] items) {
        this.items = items;
    }

    public ListArray add(final Object item) {
        final Object[] newObjects = new Object[items.length + 1];
        for (int i = 0; i < items.length; i++)
            newObjects[i] = items[i];
        newObjects[newObjects.length - 1] = item;
        return new ListArray(newObjects);
    }

    public Object get(final int i) throws InvalidIndexException {
        if (i >= 0 && i < items.length)
            return items[i];
        throw new InvalidIndexException();
    }


//    private double totalPrice(final double currentTotal, final int i) {
//        if (i == items.length)
//            return currentTotal;
//        return totalPrice(
//                currentTotal + items[i].finalPrice(),
//                i + 1);
//    }
//
//    @Override
//    public double totalPrice() {
//        return totalPrice(items[0].finalPrice(), 1);
//    }

}
