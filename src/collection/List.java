package collection;

import exceptions.InvalidIndexException;

public interface List {
    List add(Object item);
    Object get(int i) throws InvalidIndexException;

}
